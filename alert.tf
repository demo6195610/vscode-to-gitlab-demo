

resource "azurerm_monitor_action_group" "main" {
  name                = var.action_group
  resource_group_name = azurerm_resource_group.rg.name
  short_name          = var.short_name

  email_receiver {
    name          = var.reciver_name
    email_address = var.reciver_email
  }
}


resource "azurerm_monitor_activity_log_alert" "settings1" {
  name                = var.log_alert_name
  resource_group_name = azurerm_resource_group.rg.name
  scopes =  ["/subscriptions/${var.subscription_id}"]
  //scopes              =  [azurerm_resource_group.rg.id]
  description         = "This alert will monitor all administrative changes in rg."

  criteria {
         category = var.category
         level    = "Informational"
  }
  

  action {
    action_group_id = azurerm_monitor_action_group.main.id
  }
}


















/*
resource "azurerm_monitor_activity_log_alert" "settings2" {
  name                = "settings-two"
  resource_group_name = azurerm_resource_group.rg.name
  scopes              =  ["/subscriptions/${var.subscription_id}"]
  description         = "This alert will monitor all administrative changes in rg."

  criteria {
        operation_name = "Microsoft.Network/expressRouteCircuits/write"
        resource_type  = "microsoft.network/expressroutecircuits"
        category       = "Administrative"
  }
  

  action {
    action_group_id = azurerm_monitor_action_group.main.id
  }
}

resource "azurerm_monitor_activity_log_alert" "settings3" {
  name                = "settings-three"
  resource_group_name = azurerm_resource_group.rg.name
  scopes              =  ["/subscriptions/${var.subscription_id}"]
  description         = "This alert will monitor all administrative changes in rg."

  criteria {
        operation_name = "Microsoft.Network/azureFirewalls/write"
        resource_type  = "microsoft.network/azurefirewalls"
        category       = "Administrative"
  }
  

  action {
    action_group_id = azurerm_monitor_action_group.main.id
  }
}

resource "azurerm_monitor_activity_log_alert" "settings4" {
  name                = "settings-four"
  resource_group_name = azurerm_resource_group.rg.name
  scopes              =  ["/subscriptions/${var.subscription_id}"]
  description         = "This alert will monitor all administrative changes in rg."

  criteria {
        operation_name = "Microsoft.Network/ApplicationGatewayWebApplicationFirewallPolicies/write"
        resource_type  = "microsoft.network/applicationgatewaywebapplicationfirewallpolicies"
        category       = "Administrative"
  }
  

  action {
    action_group_id = azurerm_monitor_action_group.main.id
  }
}
*/